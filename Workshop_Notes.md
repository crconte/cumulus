### Recommended Folder Structure

- Create a folder for all of you inventories, and create sub-folders to contain your site-specific inventory (assuming you don't want to enable targeting of multiple sites at once).
- Split your hosts inventory into multiple files to reduce their size if needed. Ensure you control the read order of the files by giving a number prefix for your inventory file names.
- Be granular in the hierarchy, to the extent it makes sense
- Groups can have an extensive hierarchy, but group_vars folder structure doesn't support deep recursion. Flatten the group names so that you can use folder-based group_vars structuring.
  - REF:
    - [Inventory - Organizing Hosts and Group Variables](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#organizing-host-and-group-variables)
    - [Ansible Inventory Grapher](http://willthames.github.io/2014/04/03/an-ansible-inventory-grapher.html)
    - [Ansible Inventory Grapher Examples](https://serge.vanginderachter.be/2016/current-state-of-the-ansible-inventory-and-how-it-might-evolve/)

```text
.
├── inventory
│   ├── site_01
│   │   ├── 0.hosts.nwracks_01.ini
│   │   ├── 0.hosts.nwracks_02.ini
│   │   ├── 1.groups.racks.ini
│   │   └── group_vars
│   │       ├── suite1_nwracks
│   │       │   ├── auth.yaml
│   │       │   └── syslog.yaml
│   │       ├── suite1_nwracks_01
│   │       │   ├── bgp.yaml
│   │       │   └── evpn.yaml
│   │       ├── suite1_nwracks_02
│   │       │   ├── bgp.yaml
│   │       │   └── evpn.yaml
│   │       ├── suite1_svrracks
│   │       │   ├── auth.yaml
│   │       │   └── syslog.yaml
│   │       ├── suite1_svrracks_01
│   │       │   ├── bgp.yaml
│   │       │   └── evpn.yaml
│   │       └── suite1_svrracks_02
│   │           ├── bgp.yaml
│   │           └── evpn.yaml
│   └── site_02
│       ├── 0.hosts.nwracks_01.ini
│       ├── 0.hosts.nwracks_02.ini
│       ├── 1.groups.racks.ini
│       └── group_vars
│           ├── suite1_nwracks
│           │   ├── auth.yaml
│           │   └── syslog.yaml
│           ├── suite1_nwracks_01
│           │   ├── bgp.yaml
│           │   └── evpn.yaml
│           ├── suite1_nwracks_02
│           │   ├── bgp.yaml
│           │   └── evpn.yaml
│           ├── suite1_svrracks
│           │   ├── auth.yaml
│           │   └── syslog.yaml
│           ├── suite1_svrracks_01
│           │   ├── bgp.yaml
│           │   └── evpn.yaml
│           └── suite1_svrracks_02
│               ├── bgp.yaml
│               └── evpn.yaml
├── playbooks
│   ├── playbook1.yaml
│   └── playbook2.yaml
└── roles
    ├── role1
    │   └── tasks
    │       └── main.yaml
    └── role2
        └── tasks
            └── main.yaml
```
