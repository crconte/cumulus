from pybatfish.client.commands import *
from pybatfish.question.question import load_questions
from pybatfish.datamodel.flow import (HeaderConstraints,PathConstraints)
from pybatfish.question import bfq
import re

NETWORK_NAME = "cumulus"
BASE_SNAPSHOT_NAME = "cumulus"
SNAPSHOT_PATH = "/home/gitlab-runner/batfish"

load_questions()

print("[*] Initializing BASE_SNAPSHOT")
bf_set_network(NETWORK_NAME)
bf_init_snapshot(SNAPSHOT_PATH, name=BASE_SNAPSHOT_NAME, overwrite=True)

##############################################################################################################
bgpLeafStatus = bfq.bgpSessionStatus(nodes="/l/").answer().frame()

for index, row in bgpLeafStatus.iterrows():
	if(re.match(r's', str(row['Remote_Node'])) is not None):
		print("%s Has A Matching Spine BGP Neighbor" %str(row['Node']))
	else:
		raise Exception("%s Does NOT Have A Matching Spine BGP Neighbor" %str(row['Node']))

