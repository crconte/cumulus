# Virtual Machine Development Environment Configuration

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=4 orderedList=false} -->
<!-- code_chunk_output -->

- [Overview](#overview)
- [Download VirtualBox & Lubuntu 18.04](#download-virtualbox-lubuntu-1804)
- [Create Lubuntu VM](#create-lubuntu-vm)
- [VirtualBox Guest Additions and Software Updates](#virtualbox-guest-additions-and-software-updates)
- [Install Google Chrome](#install-google-chrome)
- [Install Common Utilities](#install-common-utilities)
- [Create Your SSH Key](#create-your-ssh-key)
- [Install bash-git-prompt](#install-bash-git-prompt)
- [Configure Username and Email in Git](#configure-username-and-email-in-git)
- [Install Ansible](#install-ansible)
- [Install Atom, Atom Packages, and Updates](#install-atom-atom-packages-and-updates)
- [Install Terminator](#install-terminator)
- [Mount Shared Folder](#mount-shared-folder)
- [Set Host Key Combination](#set-host-key-combination)

<!-- /code_chunk_output -->

## Overview

## Download VirtualBox & Lubuntu 18.04

- https://www.virtualbox.org/wiki/Downloads
- https://lubuntu.net/lubuntu-18-04-bionic-beaver-released/
  - http://cdimage.ubuntu.com/lubuntu/releases/18.04/release/lubuntu-18.04-desktop-amd64.iso

## Create Lubuntu VM

1.  Open VirtualBox and create a new machine

    ![](assets/img/vb-new-machine-01-create.png)

1.  Click `Expert Mode`

    ![](assets/img/vb-new-machine-02-guided-mode.png)

1.  Enter a machine name

    - Ensure `Type: Linux` and `Version: Ubuntu (64-bit)` are selected
    - Set the memory size for the new VM, 8192 to 12288 is recommended
    - Click `Create`

    ![](assets/img/vb-new-machine-03-expert-mode.png)

1.  Keep the default hard disk file type as `VDI (VirtualBox Disk Image)` and click `Expert Mode`

    ![](assets/img/vb-new-machine-04-disk-guided-mode.png)

1.  Increase the file size for your VM disk to be at least `100.00 GB` and click `Create`

    ![](assets/img/vb-new-machine-05-disk-expert-mode.png)

1.  Open `Settings` for the newly created VM

    ![](assets/img/vb-new-machine-06-settings-system.png)

1.  Click the `System` menu on the left

    - Click the `Processor` tab
    - Set the `Processor(s):` to at least `2`

    ![](assets/img/vb-new-machine-07-settings-system-cpu.png)

1.  Click the `Acceleration` tab

    - Ensure `Enable VT-x/AMD-V` is selected
    - Ensure `Enable Nested Paging` is selected

    ![](assets/img/vb-new-machine-08-settings-system-acceleration.png)

1.  Click the `Display` menu on the left

    - Set the `Video Memory:` to at least `32 MB`, preferrably `64 MB`

    ![](assets/img/vb-new-machine-09-adjust-video-mem.png)

1.  Click the `Storage` menu on the left

    - Click the `Empty` CD icon under `Controller: IDE`
    - Click the CD icon to the right of `Optical Drive: IDE Secondary Master`

    ![](assets/img/vb-new-machine-10-storage-cdrom.png)

1.  Select the Lubuntu Desktop ISO file you previous downloaded

    ![](assets/img/vb-new-machine-11-select-iso.png)

1.  Click `OK` to close the settings window

1.  Start the VM and begin the Lubuntu installation process

    The following instructions cover the non-obvious details of the installation process. Questions such as keyboard layouts and timezone settings will use their default options or options specific to your environment.

1.  Once you reach the `Updates and other software` window in the installation process

    - Select `Minimal installation`
    - Select `Download updates while install Lubuntu`
    - Select `Install third-party software for ...`
    - Click `Continue`

    ![](assets/img/vb-new-machine-12-minimal-install-with-3rd-party-sw.png)

1.  Once you reach the `Installation type` window

    - Select `Erase disk and install Lubuntu`
    - Select `Use LVM with the new Lubuntu installation`
    - Click `Install Now`

    ![](assets/img/vb-new-machine-13-erase-with-lvm.png)

1.  Once you reach the `Who are you?` window

    - Enter your `Name`, `computer's name`, and `username`
    - Set your password and ensure `Require my password to log in` is selected

    ![](assets/img/vb-new-machine-14-username-machinename.png)

1.  Complete the rest of the installation and reboot the VM

## VirtualBox Guest Additions and Software Updates

1.  Once the installation has completed and you are logged in to the Lubuntu VM

    - Click the `Devices` menu and select `Insert Guest Additions`

    ![](assets/img/vb-new-machine-15-mount-guest-additions-iso.png)

1.  Click `OK` to Open in File Manager

    ![](assets/img/vb-new-machine-16-open-fileman.png)

1.  Take note of the file path, your Guest Additions version may be different

    ![](assets/img/vb-new-machine-17-see-path.png)

1.  Open the LXTerminal by pressing `CTRL+ALT+T` or through the start menu under `System Tools\LXTerminal`

    ![](assets/img/vb-new-machine-18-open-terminal.png)

1.  Install the pre-requisites for the VirtualBox Guest Additions

    ```bash
    sudo apt update && sudo apt install -y gcc make perl
    ```

1.  Run the Guest Additions installer

    > Ensure the path below matches the path opened when the Guest Additions CD image was opened

    ```bash
    sudo /media/$USER/VBox_GAs_G.0.10/VBoxLinuxAdditions.run
    ```

1.  After the Guest Additions installer completes successfully

    - Close your LXTerminal by pressing `CTRL+D`
    - Click `Devices`, then `Optical Drives`, and then `Remove disk from virtual drive`
    - Reboot the VM

    ![](assets/img/vb-new-machine-19-unmount-guest-additions.png)

1.  After the VM reboots and you are logged in

    - Click `Devices`, then `Shared Clipboard`, then `Bidirectional` to allow for easy copy/pasting between your host machine and virtual machine

    ![](assets/img/vb-new-machine-20-shared-clipboard.png)

1.  Install all software updates by clicking the start menu, then `System Tools\Software Updater`

    ![](assets/img/vb-new-machine-21-software-updates.png)

## Install Google Chrome

1.  If you would like to install Google Chrome, click the start menu, then `Internet\Firefox Web Browser`

    ![](assets/img/vb-new-machine-22-firefox.png)

1.  Go to [http://google.com/chrome](http://google.com/chrome)

    - Click `Download Chrome`
    - Ensure `64 bit .deb` is selected
    - Click `Accept and Install`

    ![](assets/img/vb-new-machine-23-download-chrome.png)

1.  Choose `Save File` and click `OK`

    > Do _not_ choose `Open with`, the installation will not work properly

    ![](assets/img/vb-new-machine-24-save-chrome.png)

1.  Once the download completes, click the download icon and click `Open Containing Folder`

    ![](assets/img/vb-new-machine-25-open-chrome-download-folder.png)

1.  Double click the newly downloaded Chrome installer package and then click `Install Package`

    ![](assets/img/vb-new-machine-26-install-chrome.png)

1.  Once the installation is finished, you can close both windows

    ![](assets/img/vb-new-machine-27-finish-chrome-install.png)

## Install Common Utilities

Press `CTRL+ALT+T` to open the terminal and then install these commonly used utilities

```bash
sudo apt update && sudo apt install -y git curl openvpn openconnect
```

## Create Your SSH Key

1.  Create your SSH Key and copy it to GitLab

    ```bash
    ssh-keygen -t rsa -b 2048
    ```

1.  Copy the contents of `~/.ssh/id_rsa.pub` to your GitLab profile at [https://gitlab.com/profile/keys](https://gitlab.com/profile/keys)

    ```bash
    cat ~/.ssh/id_rsa.pub
    ```

## Install bash-git-prompt

The bash-git-prompt is a nice utility that give addition information related to git on your terminal's command prompt. See [https://github.com/magicmonty/bash-git-prompt](https://github.com/magicmonty/bash-git-prompt) for additional details.

```bash
git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt --depth=1
cat << EOF >> ~/.bashrc
if [ -f "$HOME/.bash-git-prompt/gitprompt.sh" ]; then
    GIT_PROMPT_ONLY_IN_REPO=1
    GIT_PROMPT_THEME=Single_line_Solarized
    source $HOME/.bash-git-prompt/gitprompt.sh
fi
EOF
```

## Configure Username and Email in Git

Configure your global user name and email settings for git

```bash
git config --global user.name "FirstName LastName"
git config --global user.email "FirstName.LastName@email.com"
```

## Install Ansible

Run the following commands to install Ansible using their published ppa repository

```bash
sudo apt update
sudo apt install -y software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install -y ansible
```

## Install Atom, Atom Packages, and Updates

Atom is a GUI editor with many packages available to make working with Ansible and other aspects of development easier. This repository contains a list of Atom packages commonly used by our team (see `apm_packages.txt`).

1.  Download and install Atom

    ```bash
    cd ~/Downloads/
    curl --location --output atom-amd64.deb https://atom.io/download/deb
    sudo dpkg --install atom-amd64.deb
    ```

1.  Fix any missing dependencies

    ```bash
    sudo apt update
    sudo apt install -f -y
    ```

1.  Install Atom packages and update them

    ```bash
    curl --output apm_packages.txt https://gitlab.com/crconte/cumulus/raw/master/apm_packages.txt
    apm install --packages-file apm_packages.txt
    apm update --no-confirm
    ```

1.  Configure the Ansible Vault package with the default path and option to use the vault password from the ansible.cfg file

    ```bash
    cat << EOF >> ~/.atom/config.cson
      "ansible-vault":
        path: "/usr/bin/ansible-vault"
        vault_password_file_flag: true
    EOF
    ```

## Install Terminator

Terminator is an alternate terminal with some nice additional features such as easy window splitting (e.g. `CTRL+SHIFT+O` and `CTRL+SHIFT+E`), and terminal grouping & command broadcasting

```bash
sudo apt update && sudo apt install -y terminator
```

## Mount Shared Folder

If you would like to easily share files between your host machine and the VM, you can mount a shared folder

1.  Add your user to the `vboxsf` group to grant your access to VirtualBox Shared Folders

    ```bash
    sudo usermod -aG vboxsf $USER
    ```

1.  Log out of Lubuntu and log back in

1.  Click `Devices`, then `Shared Folders`, and then `Shared Folders Settings`

    ![](assets/img/vb-shared-folders-01-shared-folder-settings.png)

1.  Click the plus sign icon on the right to add a new shared folder

    ![](assets/img/vb-shared-folders-02-new-shared-folder.png)

1.  In the `Folder Path:` drop down menu, select `Other...` and select a folder from your host machine to share with the VM

    ![](assets/img/vb-shared-folders-03-select-shared-folder.png)

1.  Select the `Auto-mount` option and enter the `Mount point:` as `/mnt/host_docs`, or another path of your choosing

    ![](assets/img/vb-shared-folders-04-shared-folder-settings.png)

## Set Host Key Combination

Keyboard and mouse input is captured and redirected to VM's, and the default host key combination to escape is `Right CTRL` on Windows systems. This can sometimes be a nuisance, especially if you like to use `CTRL+C`/`CTRL+V` to copy/paste. The following steps show how to change the host key combination, and we like to use `Right CTRL + Right ALT`.

1.  Click the `File` menu and then `Preferences`

![](assets/img/vb-host-key-combo-01-preferences.png)

1.  Click the `Input` menu on the left and then the `Virtual Machine` tab

    - Change the `Host Key Combination` to your desired setting

    ![](assets/img/vb-host-key-combo-02-set-host-key.png)
